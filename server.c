#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <math.h>


#define SERVERADDRESS "127.0.0.1"
#define MAXBUF 32

int main(int argc, char * argv[])
{
    int SERVERPORT=0;
    char fine[MAXBUF]="fine";
    char ast[MAXBUF]="*";
    char piu[MAXBUF]="+";
    char meno[MAXBUF]="-";
    char diviso[MAXBUF]=":";
    char uguale[MAXBUF]="=";
    int cont=0;
    int numeri[10];
    int operazione=0;
    char operazionestringa[MAXBUF];
    int i=0;
    int i2=0;
    char cancella[MAXBUF]="cancella";
    char non[MAXBUF]="non ho capito";
    int numero1;
    int numero2;
    int risultato;
    char risposta[MAXBUF];
  for(i=0;i<10;i++){
    numeri[i]=0;
    }
    int server_socket, connect_socket, retcode, nw;
    socklen_t client_addr_len;
    struct sockaddr_in server_addr, client_addr;
    char buffer[MAXBUF];
    char * client_address; // lo useremo come indirizzo del client in formato stringa
    
    while(SERVERPORT<=8000||SERVERPORT>=20000){
    
    printf("inserisci la porta di ascolto del server (8000;20000)\n");
    scanf("%d",&SERVERPORT);
    }
    
    
    // apertura socket del server
    if (((server_socket = socket(AF_INET,SOCK_STREAM,0))) == -1)
    {
        perror("Error in server socket()");
        exit(-1);
    }
        
    // preparazione dell'indirizzo locale del server
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVERPORT);
    server_addr.sin_addr.s_addr = inet_addr(SERVERADDRESS);
    
    // bind del socket
    if ((retcode = bind (server_socket, (struct sockaddr*)  &server_addr, sizeof(server_addr))) == -1)
    {
        perror("Error in server socket bind()");
        exit(-1);
    }
    
    // listen del socket
    if ((retcode = listen(server_socket, 1)) == -1)
    {
        perror("Error in server socket listen()");
        exit(-1);
    }
    
    printf("Server ready (CTRL-C quits)\n");
    
    client_addr_len = sizeof(client_addr);
    
    // cicla in attesa di connessione
    while (1)
    {
        // accetta le connessioni
        if ((connect_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len)) == -1)
        {
            perror("Error in accept()");
            close(server_socket);
            exit(-1);
        }
        
        // memorizzo l'indirizzo del client in formato stringa
        client_address = inet_ntoa(client_addr.sin_addr);
        
        printf("\nClient @ %s connects on socket %d\n", client_address, connect_socket);
        
        // riceve i messaggi e li stampa in output
        while ((retcode = read(connect_socket, buffer, MAXBUF-1)) > 0)
        {
            buffer[retcode-1] = '\0'; // buffer dev'essere una stringa
             
             numero1=atoi(buffer);
          
            
             if(numero1!=0){
             
               numeri[i2]=numero1; 
               
               if(cont==1){
                  operazione=numeri[i2-1]*numeri[i2];
                 sprintf(operazionestringa,"la moltiplicazione tra %d e %d e' %d",numeri[i2-1],numeri[i2],operazione);
                 cont=0;
                 
                }
                else if(cont==2){
               
                operazione=numeri[i2-1]+numeri[i2];
                 sprintf(operazionestringa,"l addizione tra %d e %d e' %d",numeri[i2-1],numeri[i2],operazione);
                 cont=0;
                }
                else if(cont==3){
               
                operazione=numeri[i2-1]-numeri[i2];
                 sprintf(operazionestringa,"la sottrazione tra %d e %d e' %d",numeri[i2-1],numeri[i2],operazione);
                 cont=0;
                }
                else if(cont==4){
               
                operazione=numeri[i2-1]/numeri[i2];
                 sprintf(operazionestringa,"la divisione tra %d e %d e' %d",numeri[i2-1],numeri[i2],operazione);
                 cont=0;
                }
                else
                i2++;
             }
             else if(strcmp(buffer,fine)==0){
               write(connect_socket,fine,sizeof(fine));
               exit(0);
               }
             else if(strcmp(buffer,cancella)==0){
                 system("clear");
                 for(i=0;i<10;i++){
                      numeri[i]=0;
                  }
                  i2=0;
              }
             else if(strcmp(buffer,ast)==0){
                     cont=1;
             }
             else if(strcmp(buffer,piu)==0){
                     cont=2;
             }
             else if(strcmp(buffer,meno)==0){
                     cont=3;
             }
             else if(strcmp(buffer,diviso)==0){
                     cont=4;
             }
             else if(strcmp(buffer,uguale)==0){
               write(connect_socket,operazionestringa,sizeof(operazionestringa));
               }
               
             else
                write(connect_socket,non,sizeof(non));
               
              
                  
            printf(">>%s: %s\n",client_address, buffer);
            
          
            
            
			
        }
        
        // chiudo la socket
        close(connect_socket);
    }
}
